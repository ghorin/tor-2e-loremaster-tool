import { GametoolFormApplication } from './app-gametool.js';
import { MacroUtil } from "./utils.js";

export class Gametool {

    constructor() {
        this.set_gametool();
    }

    set_gametool() {
        let macroUtil = new MacroUtil("gametool");

        let myFooter = "<img class='tor2e-gametool-footer-img' src='modules/tor-2e-gametool/images/asterism-mountains.webp'/>"  

        const template_gametool_file = "modules/tor-2e-gametool/html/gametool.html";
        const template_gametool_data = { header: "",
                                year: this.getYearCode(),
                                season : this.getSeasonCode(),
                                weather: this.getWeatherCode(),
                                pc: this.getPcCode(),
                                community: this.getCommunityCode(),
                                footer: myFooter
                            };                                
        this.gametoolForm = new GametoolFormApplication(template_gametool_data, { template: template_gametool_file, option : "" }); // data, options      
        this.gametoolForm.render(true);
    }

    getYearCode() {
        return "2956 TA";
    }
    /*
    tabs: [ { 
                label: "tab1",
                title: "My First Tab",
                content: "<em>Fancy tab1 content.</em>"
            },
            {   label: "tab2",
                title: "My Second Tab",
                content: "<em>Fancy tab2 content.</em>"
            }
        ],
    */
    getSeasonCode() {
        let arraySeasons = [ {
                                nom: "Printemps", 
                                image: "saison-printemps-"
                            },
                            { 
                                nom: "Eté", 
                                image: "saison-ete-"
                            },
                            {
                                nom: "Automne",
                                image: "saison-automne-"
                            },
                            {
                                nom: "Hiver",
                                image: "saison-hiver-"
                            }
                        ];
        arraySeasons = [ {
                            nom: "Spring", 
                            image: "saison-printemps-"
                        },
                        { 
                            nom: "Summer", 
                            image: "saison-ete-"
                        },
                        {
                            nom: "Automn",
                            image: "saison-automne-"
                        },
                        {
                            nom: "Winter",
                            image: "saison-hiver-"
                        }
                    ];                        
        let choixTheme = this.getRandomNumber(1, 2, "theme saison");
        choixTheme = 1;
        let choixSaison = this.getRandomNumber(0, 3, "saison");
        this.saison = choixSaison;
        return "<p><img class='tor2e-gametool-calendar-img' src='modules/tor-2e-gametool/images/seasons/" + arraySeasons[choixSaison].image + choixTheme + ".webp'/><br><span class='tor2e-gametool-calendar-text'>" + arraySeasons[choixSaison].nom + "</span></p>";
    }

    getWeatherCode() {
        let arrayMeteo = [ {
                                nom : "Beau",
                                image: "beau.webp"
                            }, {
                                nom: "Canicule",
                                image: "canicule.webp"
                            }, {
                                nom: "Beau et nuageeux",
                                image: "beau-nuages.webp"
                            }, {
                                nom: "Nuageux",
                                image : "nuageux.webp"
                            }, {
                                nom: "Pluvieux",
                                image: "pluvieux.webp"
                            }, {
                                nom: "Grosse pluie",
                                image: "grosse-pluie.webp"
                            }, {
                                nom: "Pluie glaciale",
                                image: "pluie-glaciale.webp"
                            }, {
                                nom: "Froid",
                                image: "froid.webp"
                            }, {
                                nom: "Neige",
                                image: "neige.webp"
                            }, {
                                nom: "Orage",
                                image: "orage.webp"
                            }, {
                                nom: "Orage et pluie",
                                image: "orage-pluie.webp"
                            }, {
                                nom: "Clair de lune",
                                image: "clair-de-lune.webp"                                              
                            }, {
                                nom: "Lune voilée",
                                image: "lune-voilee.webp"                                              
                            }
            ];
            arrayMeteo = [ {
                nom : "Beautiful",
                image: "beau.webp"
            }, {
                nom: "Heat wave",
                image: "canicule.webp"
            }, {
                nom: "Beautiful and cloudy",
                image: "beau-nuages.webp"
            }, {
                nom: "Clouds",
                image : "nuageaux.webp"
            }, {
                nom: "Rain",
                image: "pluvieux.webp"
            }, {
                nom: "Heavy rain",
                image: "grosse-pluie.webp"
            }, {
                nom: "Icy rain",
                image: "pluie-glaciale.webp"
            }, {
                nom: "Cold",
                image: "froid.webp"
            }, {
                nom: "Snow",
                image: "neige.webp"
            }, {
                nom: "Thunderstorm",
                image: "orage.webp"
            }, {
                nom: "Thunderstorm and rain",
                image: "orage-pluie.webp"
            }, {
                nom: "Moon light",
                image: "clair-de-lune.webp"                                              
            }, {
                nom: "Veiled moon",
                image: "lune-voilee.webp"                                              
            }
];

        let choixMeteo = this.getRandomNumber(0, arrayMeteo.length-1, "météo");
//        let arrayWeathers = ["01-cloud-snow", "02-cloud-rain-lightning", "03-cloud-lightning", "04-cloud-rain", "05-frozen", "06-moon-cloud-rain", "07-moon-cloud", "08-moon-stars", "09-cloud-heavy-rain", "10-clouds", "11-cloud-heavy-rain-snow", "12-cloud-rain-snow", "13-sun", "14-cloud-sun-snow", "15-cloud-sun-rain", "16-cloud-sun" ];
//        let choix = this.getRandomNumber(0, 15, "météo")
//        return "<p><img class='tor2e-gametool-calendar-img' src='modules/tor-2e-gametool/images/weather/" + arrayWeathers[choix] + ".webp'/><br><span class='tor2e-gametool-calendar-text'>Nuageux</span></p>";
        return "<p><img class='tor2e-gametool-calendar-img' src='modules/tor-2e-gametool/images/weather/" + arrayMeteo[choixMeteo].image + "'/><br><span class='tor2e-gametool-calendar-text'>" + arrayMeteo[choixMeteo].nom + "</span></p>";
    }
    getRandomNumber(min, max, use) {
        let res = Math.floor(Math.random() * (max - min + 1) ) + min;
        return res;
    }
     
    getPcCode() {
        if( !game.user.isGM ) {
            // Joueur connecté
            let myCharacter = game.user.character;
            if( myCharacter === undefined|| myCharacter === null) {
                // Pas de personnage affecté au joueur
                console.log("GAMETOOL : No Player Character attached to the player (" + game.user.name + ")");
                return "";
            } else {
                console.log("GAMETOOL : Personnage trouvé : " + myCharacter.name);
                return this.get_actor_code("fromUuidSync(\"Actor." + myCharacter.id + "\").sheet.render(true)", myCharacter.img, myCharacter.name);
            }
        } else {
            // MJ connecté
            console.log("GAMETOOL : MJ connecté");
            return "";
        }
    }

    getCommunityCode() {
        let actorCommunity = game.tor2e.macro.utility.getCommunity();
        if ( actorCommunity == null || actorCommunity === '' ) {
            console.log("GAMETOOL : No Community found => Group bar Characters isn't displayed.");
            return "";
        } else {
            console.log("GAMETOOL : Community found : " + actorCommunity.name);
            return this.get_actor_code("fromUuidSync(\"Actor." + actorCommunity.id + "\").sheet.render(true)", actorCommunity.img, actorCommunity.name);
        }
    }

    get_actor_code(macro, icone, titre) {
        return "<img src='" + icone + "' alt=\"" + titre + "\" class='tor2e-gametool-actor' onClick='" + macro + ";' aria-label=\"" + titre + "\" data-tooltip=\"" + titre + "\" /><br><span class='tor2e-gametool-calendar-text'>" + titre + "</span></p>";        
    }    
}